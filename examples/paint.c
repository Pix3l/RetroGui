/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#include <stdio.h>
#include "../rg_gui/rg.h"
#include "../rg_gui/graphic.h"
#include "../rg_gui/window.h"
#include "../rg_gui/canvas.h"

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

int main(int argc, char *argv[])
{
    // init video stuff
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0)
    {
            fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
            exit(-1);
    }
    atexit(SDL_Quit);

    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_HWSURFACE);
    if(screen == NULL)
    {
            fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
            exit(-1);
    }

    SDL_WM_SetCaption("RetroGui", "RetroGui");

    RG_widget *win;

    win = RG_createWindow(10, 10, 220, 150, NULL, NULL, "Canvas");
    RG_createCanvas(5, 20, 162, 98, 16, 10, 10, NULL, win);
    RG_createButton(5, 123, NULL, win, "Button");

    //Main game loop
    RG_app();

   	SDL_Quit();
	return 0;
}
