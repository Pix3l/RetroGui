CC = gcc
CFLAGS = -g -Wall -no-pie -DHAVE_MOUSE
LDFLAGS = -lSDL
SUBDIRS = rg_gui
ODIR = build
OBJ = main.o
OUT = main

all: $(OBJ)
	@for i in $(SUBDIRS); do \
	echo "make all in $$i..."; \
	(cd $$i; $(MAKE) all); done
	$(CC) $(CFLAGS) -o $(OUT) $(OBJ) $(SUBDIRS)/*.o $(LDFLAGS)

clean:
	@for i in $(SUBDIRS); do \
	echo "Clean all in $$i..."; \
	(cd $$i; $(MAKE) $(MFLAGS) $(MYMAKEFLAGS) clean); done
	$(RM) $(OBJ) $(OUT)
