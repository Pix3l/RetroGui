/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_MESSAGEBOX_H
#define _RG_MESSAGEBOX_H

#include <SDL/SDL.h>
#include "widget.h"

#define DEFAULT_MESSAGEBOX_H 70

typedef enum
{
    ALERT, ERROR, INFO
} MESSAGE_TYPE;

typedef struct RG_messagebox
{
    int x, y;
    //~ int icon_index;         //Used for display a system icon (Alert, Error, Info)
    unsigned int msg_type;	//Message box type (Alert, Error, Info)
    char message[150];
} RG_messagebox;

RG_widget *RG_createMessagebox(short x, short y, void(*callback) (), RG_widget *par, char *name, char *msg, int type);
void RG_doMessageBox(RG_widget *pwig);
void RG_popup(short x, short y, char *name, char *msg, int type);
void RG_drawMessageBox(RG_widget *pwig);

#endif
