/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_PALETTE_H
#define _RG_PALETTE_H

#ifndef RED
    #define RED 0xFF0000
#endif

#ifndef BLUE
    #define BLUE 0x0000FF
#endif

#ifndef GREY
    #define GREY 0xC0C0C0
#endif

#ifndef WHITE
    #define WHITE 0xFFFFFF
#endif

#ifndef BLACK
    #define BLACK 0x000000
#endif

#ifndef GREEN
    #define GREEN 0x008000
#endif

#ifndef ORANGE
    #define ORANGE 0xFF9D2E
#endif

#ifndef PURPLE
    #define PURPLE 0xFF00FF
#endif

#ifndef YELLOW
    #define YELLOW 0xFFFF00
#endif

#ifndef COLORKEY
    #define COLORKEY 0x00FF00
#endif

#ifndef SKYBLUE
    #define SKYBLUE 0x8080FF
#endif

#endif
