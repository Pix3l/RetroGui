/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_WINDOW_H
#define _RG_WINDOW_H

#include "widget.h"

#define TITLE_BG 0x525573
#define WINDOW_BG 0x2120AD

#define TITLE_SIZE 12

typedef struct RG_window
{
	int diff_x, diff_y;
	char flag_maximized;
	short _x, _y, _w, _h; // For backup use on maximize.
	char name[20];
} RG_window;

RG_widget *RG_createWindow (short x, short y, short w, short h, void(*callback) (), RG_widget *par, char *name);
void RG_doWindow(RG_widget *pwig);
void RG_drawWindow(RG_widget *pwig);

#endif
