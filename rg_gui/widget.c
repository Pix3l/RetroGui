#include <stdlib.h>

#include "rg.h"
#include "widget.h"
#include "listbox.h"
#include "graphic.h"

/**
 * Private function
 * 
 * Create an RG_Widget object
 * 
 **/
RG_widget *RG_createWidget (short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)())
{
    RG_widget *widget;
    widget = (RG_widget*) calloc(1, sizeof(RG_widget));

    if(widget == NULL)
    {
        #ifdef DEBUG_EVENT
            printf("Error creating widget\n");
        #endif
    }

    widget->next = NULL;
    widget->children = NULL;

    widget->x = x;
    widget->y = y;
    widget->w = w;
    widget->h = h;

    widget->data = data;

    widget->callback = callback;
    widget->draw = draw;

    return widget;
}

/**
 * Add a widget to the internal widget list
 *
 * @param widgetList **pList
 *      Pointer to the widget list (We can use multiple widgets lists)
 **/
RG_widget *RG_addWidget(RG_widget **pList, short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)(), RG_widget *parent)
{
    RG_widget *curr= NULL;

    /**
     * Insert to head, we don't care about parent widget if the list is empty, maybe we are in a recursion.
     **/
    if(*pList == NULL)
    {
        *pList = RG_createWidget (x, y, w, h, data, callback, draw);

        if(pList == NULL)
        {
            return NULL;
        }

        curr = *pList;

        if(parent!=NULL)
        {
            lastWidget = curr;  //Make it point to the last allocated widget (Only parents)
        }
        
        return curr;
    }

    curr = *pList;
    //Search for the last node
    while (curr!=NULL)
    {
        //If this node is a child of someone, search if curr is the parent
        if( parent!=NULL && curr == parent )
        {
            #ifdef DEBUGDEBUG_WIDGET_LIST
                printf("Found parent\n");
            #endif

            return RG_addWidget(&curr->children, x, y, w, h, data, callback, draw, parent);
        }
        
        //If the next node is empty, we exit the loop and we allocate it
        if(curr->next==NULL)
        {
            break;  //Last node
        }
        
        curr = curr->next;
    }

    /* Allocate memory for the new node and put data in it.*/
    curr->next = RG_createWidget(x, y, w, h, data, callback, draw);
    curr = curr->next;
    
    #ifdef DEBUG_EVENT
        printf("Created head widget\n");
    #endif

    return curr;
}

void RG_updateWidgets(RG_widget **pList)
{
    RG_widget *curr = *pList, 
              *prev = NULL;
    
    while (curr!=NULL)
    {
        if(curr->flags == DRAG)
        {
            //swap...
        }

        if(curr->flags == CLOSE)
        {
//            RG_cleanWidget(&curr->children);
            
            //Is head
            if (curr == *pList)
            {
                *pList = curr->next;   // Changed head
                free(curr);            // free old head
                return;
            }

            if(prev!=NULL)
            {
                /**
                 * Unlink the node from the list
                 * If curr is last node, it will always have next pointer to NULL.
                 **/
                prev->next = curr->next;
            }

            free(curr);  // Free memory
            return;
        }
        
        if(curr->update!=NULL)
        {
            curr->update(curr);
        }

        if(curr->draw!=NULL)
        {
            curr->draw(curr);
        }

        if(curr->children!=NULL)
        {
            RG_updateWidgets(&curr->children);
        }
        
        prev = curr;
        curr = curr->next;
    }
}

/**
 * Cleans the widget list, saving the only with flag_save setted to 1.
 * With parameter purge setted to 1, completely cleans list freeing up memory.
 *
 * @param EventList *pList
 *      Pointer to the events list
 * 
 * @param int purge
 *      Flag for purging also persistent events from the list
 **/
void RG_cleanWidget(RG_widget **pList)
{
    printf("Clear widgets ");
    
    RG_widget *current = *pList,
              *tail = *pList,
              *next = NULL,
              *child=NULL, *next_child=NULL;

    while(current!=NULL)
    {
        //Clean list elements
        if(current->type==LIST)
        {
            RG_listbox *data = (RG_listbox*)current->data;
            free(data->items);
            printf(".");
        }

        next=current->next;
        tail->next = next;  //Last saved next will be current next

        //Clean children
        child = current->children;
        while(child!=NULL)
        {
            next_child = child->next;
            free(child);
            child = next_child;
            printf(".");
        }

        //~ RG_cleanWidget(&current->children);   //Recursive clean children
        
        free(current);
        current=next;

        printf(".");
    }
    printf("\n");

    *pList = NULL;    //Clean head pointer

    SDL_FreeSurface(RG_icons);      //TODO move somewhere else...
}

/**
 * Draw a generic widget graphic
 **/
void RG_drawWidget(int x, int y, int w, int h)
{
    SDL_Rect rect;

    ///Widget body
    rect.x = x;
    rect.y = y;
    rect.w = w;
    rect.h = h;

    SDL_FillRect(screen, &rect, 0xaaaaaa);

    ///White borders
    //Horizontal
    rect.x = x;
    rect.y = y;
    rect.w = w-1;
    rect.h = 1;
    SDL_FillRect(screen, &rect, 0xffffff);

    //Vertical
    rect.x = x;
    rect.y = y;
    rect.w = 1;
    rect.h = h-1;
    SDL_FillRect(screen, &rect, 0xffffff);

    ///Gray borders
    //Horizontal
    rect.x = x+1;
    rect.y = y+h-1;
    rect.w = w-1;
    rect.h = 1;
    SDL_FillRect(screen, &rect, 0x6d6d6d);

    //Vertical
    rect.x = x+w-1;
    rect.y = y;
    rect.w = 1;
    rect.h = h-1;
    SDL_FillRect(screen, &rect, 0x6d6d6d);
}
