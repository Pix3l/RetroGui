/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_MOUSE_H
#define _RG_MOUSE_H

#include <SDL/SDL.h>
#include "bit.h"

typedef enum _RG_Mouse_flags {
    mouse_hook          = 1 << 0,  // 000001  1
    //~ menu_show_title     = 1 << 1,  // 000010  2
    //~ menu_show_border    = 1 << 2,  // 000100  4
    //~ menu_persistent     = 1 << 3,  // 000100
    //~ flag     = 8     // 001000  8
} RG_Mouse_flags;

struct _RG_Mouse
{
	int x, y;
    int leftButton, rightButton;
    int flags;
} RG_Mouse;

void handle_mouse(void);

#endif
