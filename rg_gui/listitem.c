/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "widget.h"
#include "listitem.h"
#include "listbox.h"
#include "font.h"

/**
 * Align a ListItem to their parent widget
 * 
 * @param RG_widget *pwig
 * 		The parent widget
 * 
 * @param WIDGET_TYPE type
 * 		The type of the widget
 **/
void alignListItems(RG_widget *pwig, WIDGET_TYPE type)
{
    void *data = NULL;
	
    //Retrive object data by casting
    switch(type)
    {
        case LIST:
            *data = (RG_listbox*)pwig->data;
            break;
        case COMBO:
            //~ *data = (RG_combobox*)pwig->data;
            break;
    }

    int i;
    for(i = 0; i<data->num_items; i++)
    {
            data->items[i].x = 1+pwig->x;
            data->items[i].y = 1+pwig->y+(i*RG_CHAR_H);
    }
}
