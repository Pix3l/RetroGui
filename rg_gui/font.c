/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "font.h"
#include "bit.h"
#include "graphic.h"

static char fontArray[8][759] = {
  "           xx    xx xx   xx xx     xx             xxx     xx       xx    xx                                                  xx  xxxxx    xx     xxxx    xxxx      xxx  xxxxxx    xxx   xxxxxx   xxxx    xxxx                      xx            xx       xxxx   xxxxx    xx    xxxxxx    xxxx  xxxxx   xxxxxxx xxxxxxx   xxxx  xx  xx   xxxx      xxxx xxx  xx xxxx    xx   xx xx   xx   xxx   xxxxxx   xxxxx  xxxxxx   xxxxx  xxxxxx  xx  xx  xx  xx  xx   xx xx   xx xx  xx  xxxxxxx  xxxx   xx       xxxx      x              xx            xxx                xxx            xxx           xxx       xx        xx  xxx      xxx                                                               x                                                       xxx     xx   xxx     xxx   x",
  "          xxxx   xx xx   xx xx   xxxxxx xx   xx  xx xx    xx      xx      xx     xx  xx    xx                               xx  xx  xxx  xxx    xx  xx  xx  xx    xxxx  xx       xx     xx  xx  xx  xx  xx  xx     xx      xx     xx              xx     xx  xx xx   xx  xxxx    xx  xx  xx  xx  xx xx   xx   x  xx   x  xx  xx xx  xx    xx        xx   xx  xx  xx     xxx xxx xxx  xx  xx xx   xx  xx xx   xx  xx  xx xx   xx x xx x  xx  xx  xx  xx  xx   xx xx   xx xx  xx  xx   xx  xx      xx        xx     xxx             xx             xx                 xx           xx xx           xx                      xx       xx                                                              xx                                                      xx       xx     xx   xx xx xx",
  "          xxxx   xx xx  xxxxxxx xx      xx  xx    xxx    xx      xx        xx     xxxx     xx                              xx   xx xxxx   xx        xx      xx   xx xx  xxxxx   xx          xx  xx  xx  xx  xx     xx      xx    xx      xxxxxx    xx       xx  xx xxxx xx  xx   xx  xx xx       xx  xx  xx x    xx x   xx      xx  xx    xx        xx   xx xx   xx     xxxxxxx xxxx xx xx   xx  xx  xx xx   xx  xx  xx xxx       xx    xx  xx  xx  xx  xx   xx  xx xx  xx  xx  x   xx   xx       xx       xx    xx xx             xx    xxxx    xx      xxxx       xx   xxxx    xx  x   xxx xx  xx xx   xxx       xxx   xx  xx   xx    xx  xx  x xxx    xxxx   xx xxx   xxx xx xx xxx   xxxxx  xxxxxx  xx  xx  xx  xx  xx   xx xx   xx xx  xx  xxxxxx    xx       xx     xx   x   xxx ",
  "           xx            xx xx   xxxxx     xx    xxx xx          xx        xx   xxxxxxxx xxxxxx          xxxxxx           xx    xxxx xx   xx      xxx     xxx   xx  xx      xx  xxxxx      xx    xxxx    xxxxx                  xx                  xx     xx   xx xxxx xx  xx   xxxxx  xx       xx  xx  xxxx    xxxx   xx      xxxxxx    xx        xx   xxxx    xx     xxxxxxx xx xxxx xx   xx  xxxxx  xx   xx  xxxxx   xxxx     xx    xx  xx  xx  xx  xx   xx   xxx    xxxx      xx    xx        xx      xx   xx   xx                     xx   xxxxx  xx  xx   xxxxx  xx  xx  xxxx    xx  xx   xxx xx   xx        xx   xx xx    xx    xxxxxxx xx  xx  xx  xx   xx  xx xx  xx   xxx xx xx        xx    xx  xx  xx  xx  xx   xx  xx xx  xx  xx  x  xx   xxx                xxx         ",
  "           xx           xxxxxxx      xx   xx    xx xxx           xx        xx     xxxx     xx                            xx     xxx  xx   xx     xx         xx  xxxxxxx     xx  xx  xx    xx    xx  xx      xx                   xx      xxxxxx    xx      xx   xx xxx  xxxxxx   xx  xx xx       xx  xx  xx x    xx x   xx  xxx xx  xx    xx    xx  xx   xx xx   xx   x xx x xx xx  xxx xx   xx  xx     xx x xx  xx xx      xxx   xx    xx  xx  xx  xx  xx x xx  xx xx    xx      xx  x  xx         xx     xx                            xxxxx   xx  xx xx      xx  xx  xxxxxx   xx     xx  xx   xx  xx   xx        xx   xxxx     xx    xxxxxxx xx  xx  xx  xx   xx  xx xx  xx   xx   x  xxx      xx    xx  xx  xx  xx  xx x xx   xxx   xx  xx    xx      xx       xx     xx           ",
  "                         xx xx  xxxxxx   xx  xx xx  xx            xx      xx     xx  xx    xx      xx              xx   xx      xx   xx   xx    xx  xx  xx  xx      xx  xx  xx  xx  xx    xx    xx  xx     xx      xx      xx     xx              xx            xx      xx  xx   xx  xx  xx  xx  xx xx   xx   x  xx      xx  xx xx  xx    xx    xx  xx   xx  xx  xx  xx xx   xx xx   xx  xx xx   xx      xxxxx   xx  xx xx   xx   xx    xx  xx   xxxx   xxxxxxx xx   xx   xx     xx  xx  xx          xx    xx                           xx  xx   xx  xx xx  xx  xx  xx  xx       xx      xxxxx   xx  xx   xx    xx  xx   xx xx    xx    xx x xx xx  xx  xx  xx   xxxxx   xxxxx   xx        xxx    xx x  xx  xx   xxxx   xxxxxxx  xx xx   xxxxx   xx  x    xx       xx     xx           ",
  "           xx            xx xx     xx   xx   xx  xxx xx            xx    xx                        xx              xx   x        xxxxx  xxxxxx  xxxxxx   xxxx      xxxx  xxxx    xxxx     xx     xxxx    xxx       xx      xx      xx            xx        xx    xxxxx  xx  xx  xxxxxx    xxxx  xxxxx   xxxxxxx xxxx      xxx x xx  xx   xxxx    xxxx   xxx  xx xxxxxxx xx   xx xx   xx   xxx   xxxx        xxx xxx  xx  xxxxx   xxxx   xxxxxx    xx     xx xx  xx   xx  xxxx   xxxxxxx  xxxx         x  xxxx                            xxx xx xx xxx   xxxx    xxx xx  xxxx   xxxx        xx  xxx  xx  xxxx   xx  xx  xxx  xx  xxxx   xx x xx xx  xx   xxxx    xx         xx  xxxx    xxxxx      xx    xxx xx   xx     xx xx  xx   xx     xx  xxxxxx     xxx     xx   xxx            ",
  "                                                                                                  xx                                                                                                                      xx                                                                                                                                                                                                                                                                                            xxxxxxx                                                         xxxxx                    xxxx                                           xxxx       xxxx                                                         xxxxx                                          "
};

//Private functions
char *RG_substring(char *string, int position, int length);
void RG_appendChar(char *str, char ch, int length);

void RG_font_init()
{
	//Init the built-in font
	/*
	int x, y;
	for (y = 0; y < FONT_ARRAY_H; y ++)
	{
		for (x = 0; x < FONT_ARRAY_W; x ++)
		{
			if(fontArray[y][x]=='x')
			{
				put_pixel(RG_font, x, y, 0xff0000);
			}
			
			//~ printf("%c",fontArray[y][x]);
		}
		
		//~ printf("\n");
	}
	*/
}

char *RG_substring(char *string, int position, int length)
{
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
 
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }
 
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *string;      
      string++;   
   }
 
   *(pointer+c) = '\0';
 
   return pointer;
}

void RG_appendChar(char *str, char ch, int length)
{
    size_t len = strlen(str);
    
    if((len+1) <= length)
    {
        str[len] = ch;
        str[len+1] = '\0';
    }

	#ifdef DEBUG
		printf("debug %s\n", str);
	#endif

}

void RG_textInput(char *string, int length)
{
	SDL_EnableUNICODE( SDL_ENABLE );

	//~ if( event.type == SDL_KEYDOWN )
	//~ {
		//Append a character if available
		if( ( input_event.key.keysym.unicode >= (Uint16)' ' ) && ( input_event.key.keysym.unicode <= (Uint16)'~' ) )
		{
			//Append the character
			RG_appendChar(string, (char)input_event.key.keysym.unicode, length);
		}

		RG_eraseInput(string);

		//Stop key repeating!
		input_event.key.keysym.unicode = 0;
	//~ }
}

void RG_putChar(int dest_x, int dest_y, int asciicode, int color)
{
	//Start read the font matrix from the correct ascii character
	int start_x = ((asciicode-32)*8)-1;
	
	int x, y;
	for (y = 0; y < 8; y++)
	{
		for (x = 0; x < 8; x++)
		{
			if(fontArray[y][start_x+x]=='x')
			{
				put_pixel(screen, dest_x+x, dest_y+y, color);
			}
			//This put a background color...
			//~ else
			//~ {
				//~ put_pixel(dest, dest_x+x, dest_y+y, 0x00ff00);
			//~ }
		}
	}
}

void RG_putString(int dest_x, int dest_y, char *text, int color)
{
	//For the entire string
	int i=0;
	while(text[i] != '\0')
	{
		RG_putChar(dest_x, dest_y, text[i], color);
		dest_x+=RG_CHAR_W;
		i++;
	}
}

void RG_eraseInput(char *string)
{
	//If backspace was pressed and the string isn't blank
	if( ( input_event.key.keysym.sym == SDLK_BACKSPACE ) && ( strlen(string) != 0 ) )
	{
		//Remove a character from the end
		//~ str.erase( str.length() - 1 );
		char *tmp;
		
		tmp = RG_substring(string, 0, strlen(string)-1);
		strcpy(string, tmp);
		
		input_event.key.keysym.sym = 0;
	}
}
