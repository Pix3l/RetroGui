#ifndef _RG_EDITFIELD_H
#define _RG_EDITFIELD_H

#include "widget.h"

#define EDITFIELD_BG 0x525573
#define DEFAULT_EDITFIELD_H 20

typedef struct RG_editfield
{
	int x, y;
	char flag_active;
	char label[20];
	char store[30]; 	//input data storer
} RG_editfield;

typedef enum
{
    TEXFIELD, NUMFIELD
} RGWIDGET_EDITFIELD_TYPE;

RG_widget *RG_createEditField (short x, short y, void(*callback) (), RG_widget *par, char *label, int type);
void RG_doEditField(RG_widget *pwig);
void RG_drawEditField(RG_widget *pwig);
void RG_drawField(int x, int y, int w, int h);

#endif
