/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_RG_CHAR_H
#define _RG_RG_CHAR_H

#define RG_CHAR_W 8
#define RG_CHAR_H 16

#define FONT_ARRAY_W 764
#define FONT_ARRAY_H 22

SDL_Surface *RG_font, *RG_tmpfont;

void RG_font_init(void);
void RG_textInput(char *string, int length);
void RG_putChar(int dest_x, int dest_y, int asciicode, int color);
void RG_putString(int dest_x, int dest_y, char *text, int color);
void RG_eraseInput(char *string);

#endif
