#ifndef _RG_WIDGET_H
#define _RG_WIDGET_H

#include <SDL/SDL.h>

SDL_Surface *RG_icons;
#define RG_ICON_W 8
#define RG_ICON_H 8

typedef enum
{
    WINDOW, BUTTON, LIST, COMBO, LISTITEM
} RGWIDGET_TYPE;

typedef enum
{
    MINIMIZE, MAXIMIZE, CLOSE, DRAG
} WIDGET_FLAGS;

typedef struct _RG_widget
{
    short x, y, w, h;
    short id;
    short flags;
    unsigned short depth;
    void (*update)(struct _RG_widget *pwig);	//Update widget status
    void (*callback)();		//Let the widget do something
    void (*draw)(struct _RG_widget *pwig);
    //~ void (*destroy)(struct _RG_widget *pwig);
    short type;
    void *data;	/// Pointer to more object data...
    struct _RG_widget *parent;
    struct _RG_widget *next;
    struct _RG_widget *children;
} RG_widget;

RG_widget *widgetList,   //Default widget list
          *lastWidget;   //Last created widget (Only parents)

RG_widget *RG_addWidget(RG_widget **pList, short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)(), RG_widget *parent);
void RG_updateWidgets(RG_widget **pList);
void swapWidget(RG_widget *pwig);
void RG_cleanWidget(RG_widget **pList);
void RG_drawWidget(int x, int y, int w, int h);
void RG_Render(void);

#endif
