/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 *              This source is part of project RetroGear               *
 *                  https://gitlab.com/Pix3l/RetroGear                 *
 *                                                                     *
 **********************************************************************/

#ifndef _BIT_H
#define _BIT_H

int hasFlag(int flag_store, unsigned char flags);
void setFlag(int *flag_store, unsigned char flags);
void unsetFlag(int *flag_store, unsigned char flags);
void toggleFlag(int *flag_store, unsigned char flags);


#endif
