/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_BUTTON_H
#define _RG_BUTTON_H

#include <SDL/SDL.h>
#include "widget.h"

#define BUTTON_BG 0x525573
#define DEFAULT_BUTTON_H 20

#define TITLE_SIZE 12

typedef struct RG_button
{
	int x, y;
	char flag_clicked;
	char text[20];
	int icon_index;
	//~ SDL_Surface *image;
} RG_button;

RG_widget *RG_createButton (short x, short y, void(*callback) (), RG_widget *par, char *text);
RG_widget *RG_createImageButton (short x, short y, void(*callback) (), RG_widget *par, int icon_index);
void RG_doButton(RG_widget *pwig);
void RG_drawButton(RG_widget *pwig);

#endif
