/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#ifndef _RG_LISTBOX_H
#define _RG_LISTBOX_H

#include <SDL/SDL.h>
#include "widget.h"
#include "listitem.h"

#define DEFAULT_LISTBOX_W 100

typedef struct RG_listbox
{
	int x, y;
	char flag_clicked;
	char label[20];
	int num_items;
	int curr_item;	//selected item
	RG_listitem *items;
} RG_listbox;

RG_widget *RG_createListBox (short x, short y, RG_widget *par, char *label);
void RG_addListBoxItem(RG_widget *pwig, char *text);
void RG_drawListBox(RG_widget *pwig);

#endif
