/***********************************************************************
 *                                                                     *
 *                              RetroGui                               *
 *                                                                     *
 *                     SDL based simple GUI library                    *
 *                                                                     *
 *     This software is released as open source under GPL license      *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it          *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                 *
 *                                                                     *
 *               This software is provided without warranty,           *
 *                       use it at your own risk!                      *
 *                                                                     *
 * 					Copyright Cerullo Davide, 2013-2018                *
 *                     pix3lworkshop.altervista.org                    *
 *                  https://gitlab.com/Pix3l/RetroGui                  *
 *                                                                     *
 **********************************************************************/

#include "rg.h"
#include "mouse.h"

/**
 * Handles mouse events enhancing the virtual mouse
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_mouse(void)
{
    if(input_event.type == SDL_MOUSEMOTION)
    {
        // Update mouse position
        RG_Mouse.x = input_event.motion.x;
        RG_Mouse.y = input_event.motion.y;

        #ifdef DEBUG_VMOUSE
            printf("Mouse coordinates %d,%d\n", RG_Mouse.x, RG_Mouse.y);
        #endif
    }

    if(input_event.type == SDL_MOUSEBUTTONDOWN)
    {
        if (input_event.button.button == 1)
        {
            RG_Mouse.leftButton = 1;

            #ifdef DEBUG_VMOUSE
                printf("Mouse left button pressed\n");
            #endif
        }

        if (input_event.button.button == 3)
        {
            RG_Mouse.rightButton = 1;

            #ifdef DEBUG_VMOUSE
                printf("Mouse right button pressed\n");
            #endif
        }
    }
    
    if(input_event.type == SDL_MOUSEBUTTONUP)
    {
        // update button down state if left-clicking
        if (input_event.button.button == 1)
        {
            RG_Mouse.leftButton = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Mouse left button pressed\n");
            #endif
        }

        if (input_event.button.button == 3)
        {
            RG_Mouse.rightButton = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Mouse right button pressed\n");
            #endif
        }
    }
}
